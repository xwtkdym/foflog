/**
 * @file foflog.hpp
 * @author xwtkdym (xwtkdym@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-16
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef FOFLOG_FOFLOG_HPP_
#define  FOFLOG_FOFLOG_HPP_

#include <spdlog/spdlog.h>
#include <spdlog/sinks/rotating_file_sink.h>

#if defined(__ANDROID__)
#   include <spdlog/sinks/android_sink.h>
#endif

#include <string>

namespace foflog
{

namespace level
{

#if defined(ERROR)
#   undef ERROR
#endif
enum LogLevel {
    ALL         = 1 << 0,
    TRACE       = 1 << 1,
    DEBUG       = 1 << 2,
    INFO        = 1 << 3,
    WARN        = 1 << 4,
    ERROR       = 1 << 5,
    FATAL       = 1 << 6,
    OFF         = 1 << 7,
};

}; // namespace level

inline spdlog::level::level_enum ConvertLevelToSPDLOG(enum level::LogLevel level)
{
    switch(level) {
    case level::ALL:
        return spdlog::level::trace;
        break;
    case level::TRACE:
        return spdlog::level::trace;
        break;
    case level::DEBUG:
        return spdlog::level::debug;
        break;
    case level::INFO:
        return spdlog::level::info;
        break;
    case level::WARN:
        return spdlog::level::warn;
        break;
    case level::ERROR:
        return spdlog::level::err;
        break;
    case level::FATAL:
        return spdlog::level::critical;
        break;
    case level::OFF:
        return spdlog::level::off;
        break;
    default:
        return spdlog::level::trace;
        break;
    }
}



template<typename T, typename... Args>
inline void Log(enum level::LogLevel loglevel, const T& fmt, Args&&... args)
{
    spdlog::log(ConvertLevelToSPDLOG(loglevel), fmt, std::forward<Args>(args)...);
}

template<typename T, typename... Args>
inline void Trace(const T& fmt, Args&&... args)
{
    Log(level::TRACE, fmt, std::forward<Args>(args)...);
}

template<typename T, typename... Args>
inline void Debug(const T& fmt, Args&&... args)
{
    Log(level::DEBUG, fmt, std::forward<Args>(args)...);
}

template<typename T, typename... Args>
inline void Info(const T& fmt, Args&&... args)
{
    Log(level::INFO, fmt, std::forward<Args>(args)...);
}

template<typename T, typename... Args>
inline void Warn(const T& fmt, Args&&... args)
{
    Log(level::WARN, fmt, std::forward<Args>(args)...);
}

template<typename T, typename... Args>
inline void Error(const T& fmt, Args&&... args)
{
    Log(level::ERROR, fmt, std::forward<Args>(args)...);
}

template<typename T, typename... Args>
inline void Fatal(const T& fmt, Args&&... args)
{
    Log(level::FATAL, fmt, std::forward<Args>(args)...);
}


template<typename T>
void Log(enum level::LogLevel loglevel, const T& msg)
{
    spdlog::log(ConvertLevelToSPDLOG(loglevel), msg);
}

template<typename T>
inline void Trace(const T& msg)
{
    Log(level::TRACE, msg);
}

template<typename T>
inline void Debug(const T& msg)
{
    Log(level::DEBUG, msg);
}

template<typename T>
inline void Info(const T& msg)
{
    Log(level::INFO, msg);
}

template<typename T>
inline void Warn(const T& msg)
{
    Log(level::WARN, msg);
}

template<typename T>
inline void Error(const T& msg)
{
    Log(level::ERROR, msg);
}

template<typename T>
inline void Fatal(const T& msg)
{
    Log(level::FATAL, msg);
}

inline void SetPattern(const std::string& pattern)
{
    spdlog::set_pattern(pattern);
}

inline void SetLevel(enum level::LogLevel loglevel)
{
    spdlog::set_level(ConvertLevelToSPDLOG(loglevel));
}

inline void Shutdown()
{
    spdlog::flush_on(spdlog::level::trace);
    spdlog::shutdown();
}


inline void Init(const std::string& logpath, enum level::LogLevel level = level::ALL)
{
    auto rotateSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(logpath, 1 * 1024 * 1024, 3, true);

#if defined(__ANDROID__)
    std::string tag = "native-log";
    auto androidSink = std::make_shared<spdlog::sinks::android_sink_mt>(tag);
    spdlog::logger logger("native_logger", {rotateSink, androidSink});
#else
    spdlog::logger logger("native_logger", {rotateSink});
#endif

    spdlog::set_default_logger(std::make_shared<spdlog::logger>(logger));
    spdlog::flush_every(std::chrono::seconds(10));

    foflog::SetPattern("[%H:%M:%S %z] [%^%L%$] [thread %t] %v");
    foflog::SetLevel(level);
    foflog::Info("foflog Init(logdir: {}, loglevel: {}) Start", logpath, (int)level);
}

};

#endif // FOFLOG_FOFLOG_HPP_