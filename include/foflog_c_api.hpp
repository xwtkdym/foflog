/**
 * @file foflog_c_api.hpp
 * @author xwtkdym (xwtkdym@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef FOFLOG_FOFLOG_C_API_HPP_
#define FOFLOG_FOFLOG_C_API_HPP_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef FOFLOG_API
#if defined(foflog_SHARED_LIB)
#   if defined(_WIN32)
#      if defined(foflog_EXPORTS)
#          define FOFLOG_API __declspec(dllexport)
#      else // !foflog_EXPORTS
#          define FOFLOG_API __declspec(dllimport)
#      endif
#   else // !defined(_WIN32)
#       define FOFLOG_API __attribute__((visibility("default")))
#   endif
#else // !defined(foflog_SHARED_LIB)
#   define FOFLOG_API
#endif
#endif // FOFLOG_API

FOFLOG_API extern void FOFLOG_SetPatternCAPI(const char* pattern);

FOFLOG_API extern void FOFLOG_SetLevelCAPI(int level);

FOFLOG_API extern void FOFLOG_ShutdownCAPI();

FOFLOG_API extern void FOFLOG_InitCAPI(const char* logdir, int level);

#ifdef __cplusplus 
}
#endif

#endif // FOFLOG_FOFLOG_C_API_HPP_