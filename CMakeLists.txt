cmake_minimum_required(VERSION 3.18)


project(foflog LANGUAGES C CXX)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(MACOSX_RPATH True)
endif()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(foflog_SHARED_LIB "build shared lib?" ON)
if (foflog_SHARED_LIB)
    add_compile_definitions(foflog_SHARED_LIB foflog_EXPORTS)
endif()

add_library(
    foflog
    SHARED
    src/foflog_c_api.cpp
)

target_include_directories(
    foflog
    PRIVATE
    include
    ${spdlog_SOURCE_DIR}/include
)

if (${CMAKE_SYSTEM_NAME} MATCHES "Android")
    target_link_libraries(
        foflog
        PRIVATE
        log
    )
endif()

install(
    TARGETS foflog
    EXPORT foflog
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)


add_executable(
    foflog_test
    src/foflog_test.cpp
)

target_include_directories(
    foflog_test
    PRIVATE
    include
    ${spdlog_SOURCE_DIR}/include
)

target_link_libraries(
    foflog_test
    PRIVATE
    foflog
)

if (${CMAKE_SYSTEM_NAME} MATCHES "Android")
    target_link_libraries(
        foflog_test
        PRIVATE
        log
    )
endif()