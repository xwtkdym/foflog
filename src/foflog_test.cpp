/**
 * @file foflog_test.cpp
 * @author xwtkdym (xwtkdym@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <foflog.hpp>
extern "C" {
#include <foflog_c_api.hpp>
}

#include <string>
#include <functional>


int main(int argc, char** argv)
{
    // foflog::Init("./logdir/test.log");
    FOFLOG_InitCAPI("./logdir/test.log", 0);
    foflog::Info("test");
    foflog::Info("test", "test");
    foflog::Info("test {} {}", 1, std::string("test"));
    std::string test("test");
    foflog::Info("test", test);

    foflog::Error("Dasf");
    foflog::Fatal("Dasf");
    std::string k(1000, 'a');
    for (int i = 0; i < 100000; ++i) {
        foflog::Info(k);
    }
    foflog::Shutdown();

    return 0;
}