/**
 * @file foflog_c_api.cpp
 * @author xwtkdym (xwtkdym@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <foflog.hpp>
#ifdef __cplusplus
extern "C" {
#endif
#include <foflog_c_api.hpp>
#ifdef __cplusplus
}
#endif

void FOFLOG_SetPatternCAPI(const char* pattern)
{
    foflog::SetPattern(pattern);
}

void FOFLOG_SetLevelCAPI(int level)
{
    foflog::SetLevel((foflog::level::LogLevel)level);
}

void FOFLOG_ShutdownCAPI()
{
    foflog::Shutdown();
}

void FOFLOG_InitCAPI(const char* logpath, int level)
{
    foflog::Init(logpath, (foflog::level::LogLevel)level);
}